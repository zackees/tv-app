

Oct. 2019 - Mark Harding <mark@minds.com>:

A key expectation from our end is the ability to have a POC that can be plugged into a TV via a HDMI cable and play Minds videos.

As we discussed on our call a few ago, I think using the Minds.com frontend does NOT make sense. Instead I think it would be much better to spend your time creating a very simple angular application that interfaces with our current API.

Most of us are using Macs. We do have Raspberry Pi's available too, which I think would be the preferred option.