# Minds.com cobalt compatibility
report by Zach Vorhies – Oct 16th, 2019

## Overview
Cobalt for linux was built from open source and placed into an Ubuntu VM which was run on a host mac.

I ran the cobalt engine for each of minds.com, minds.tv and subverse.net and logged the compatibility issues. I then aggregated these issues.

Each report will be in csv format with the first column being the issue name and the second column being the number of times this issue was logged.

## Results + Recommendations:

Only minds.tv loaded correctly while subverse.net and minds.com was not able to load a homescreen. It was recommended by the minds.com team that a new video app be created that plugs into an existing minds API. I agree that this is probably the cleanest way to proceed, given that number of html-dom and css issues.
Lets continue that conversation for the next milestone.

# Compatibility Report
#### Minds.com

```
"unsupported property value for font",7273
"unsupported pseudo-class",446
"unsupported value",389
"unsupported pseudo-element",384
"invalid qualified rule",144
"unsupported property value for animation",49
"invalid transform function",14
"invalid declaration",13
"non-zero length is not allowed without unit identifier",11
"unsupported selector within :not()",9
"warning: unsupported value",3
"length value must not be negative",3
"invalid rule @supports",3
"warning: unsupported property user-select",1
"warning: unsupported property stroke-width",1
"warning: unsupported property stroke-dasharray",1
"warning: unsupported property stroke",1
"warning: unsupported property shape-rendering",1
"warning: unsupported property hyphens",1
"warning: unsupported property float",1
"warning: unsupported property fill",1
"warning: unsupported property direction",1
"warning: unsupported property cursor",1
"warning: unsupported property box-sizing",1
"warning: invalid transform function",1
"unsupported property zoom",1
"unsupported property word-spacing",1
"unsupported property word-break",1
"unsupported property will-change",1
"unsupported property widows",1
"unsupported property transform-style",
"unsupported property touch-action",1
"unsupported property text-rending",1
"unsupported property text-rendering",1
"unsupported property table-layout",1
"unsupported property speak",1
"unsupported property scrollbar-width",1
"unsupported property resize",1
"unsupported property quotes",1
"unsupported property perspective-origin",1
"unsupported property page-break-inside",1
"unsupported property page-break-after",1
"unsupported property overflow-y",1
"unsupported property overflow-x",1
"unsupported property orphans",1
"unsupported property object-position",1
"unsupported property object-fit",1
"unsupported property max-heigh",1
"unsupported property mask",1
"unsupported property margin-botom",1
"unsupported property list-style-type",1
"unsupported property list-style",1
"unsupported property line-clamp",1
"unsupported property letter-spacing",1
"unsupported property justify-self",1
"unsupported property justify-items",1
"unsupported property grid-template-rows",1
"unsupported property grid-template-columns",1
"unsupported property grid-row",1
"unsupported property grid-gap",1
"unsupported property grid-column",1
"unsupported property grid-auto-rows",1
"unsupported property font-weigth",1
"unsupported property font-variant",1
"unsupported property font-feature-settings",1
"unsupported property font-display",1
"unsupported property clear",1
"unsupported property border-collapse",1
"unsupported property background-origin",1
"unsupported property background-clip",1
"unsupported property background-blend-mode",1
"unsupported property background-attachment",1
"unsupported property appearance",1
"unrecoverable syntax error",1
"negative values of percentage are illegal",1
"invalid rule @charset",1
"invalid box shadow property.",1
```

#### Minds.tv

```
"unsupported property value for font",6835
"unsupported value",102
"non-zero length is not allowed without unit identifier",94
"unsupported pseudo-class",72
"integer value must be positive",59
"invalid declaration",36
"unsupported pseudo-element",21
"length value must not be negative",21
"invalid qualified rule",21
"invalid rule @supports",12
"unsupported selector within :not()",4
"unsupported property value for animation",4
"Unexpected end tag : scr",4
"End tag : expected >",4
"warning: unsupported pseudo-class",3
"warning: unsupported value",1
"warning: invalid qualified rule",1
"unsupported property zoom",1
"unsupported property word-break",1
"unsupported property value for font-weight",1
"unsupported property unicode-bidi",1
"unsupported property text-rendering",1
"unsupported property table-layout",1
"unsupported property speak",1
"unsupported property overflow-y",1
"unsupported property object-fit",1
"unsupported property list-style-type",1
"unsupported property list-style",1
"unsupported property letter-spacing",1
"unsupported property hyphens",1
"unsupported property grid-template-rows",1
"unsupported property grid-template-columns",1
"unsupported property grid-template-areas",1
"unsupported property grid-area",1
"unsupported property font-variant",1
"unsupported property float",1
"unsupported property fill",1
"unsupported property direction",1
"unsupported property cursor",1
"unsupported property clip-path",1
"unsupported property clear",1
"unsupported property caption-side",1
"unsupported property box-sizing",1
"unsupported property border-spacing",1
"unsupported property border-collapse",1
"unsupported property background-attachment",1
"unsupported property backface-visibility",1
"unrecoverable syntax error",1
"invalid transform function",1
"invalid rule @viewport",1
"invalid rule @charset",1
"font-style value declared twice in font.",1
```

#### Subverse.net

```
"unsupported property value for font",7273
"unsupported pseudo-class",460
"unsupported value",400
"unsupported pseudo-element",384
"invalid qualified rule",145
"unsupported property value for animation",53
"invalid transform function",14
"non-zero length is not allowed without unit identifier",13
"invalid declaration",13
"unsupported selector within :not()",9
"warning: unsupported value",3
"length value must not be negative",3
"invalid rule @supports",3
"warning: unsupported property user-select",1
"warning: unsupported property stroke-width",1
"warning: unsupported property stroke-dasharray",1
"warning: unsupported property stroke",1
"warning: unsupported property shape-rendering",1
"warning: unsupported property hyphens",1
"warning: unsupported property float",1
"warning: unsupported property fill",1
"warning: unsupported property direction",1
"warning: unsupported property cursor",1
"warning: unsupported property box-sizing",1
"warning: invalid transform function",1
"unsupported property zoom",1
"unsupported property word-spacing",1
"unsupported property word-break",1
"unsupported property will-change",1
"unsupported property widows",1
"unsupported property transform-style",1
"unsupported property touch-action",1
"unsupported property text-rending",1
"unsupported property text-rendering",1
"unsupported property table-layout",1
"unsupported property speak",1
"unsupported property scrollbar-width",1
"unsupported property scroll-snap-type",1
"unsupported property scroll-snap-align",1
"unsupported property resize",1
"unsupported property quotes",1
"unsupported property perspective-origin",1
"unsupported property page-break-inside",1
"unsupported property page-break-after",1
"unsupported property padding-inline-start",1
"unsupported property overflow-y",1
"unsupported property overflow-x",1
"unsupported property orphans",1
"unsupported property object-position",1
"unsupported property object-fit",1
"unsupported property max-heigh",1
"unsupported property mask",1
"unsupported property margin-botom",1
"unsupported property margin-block-end",1
"unsupported property list-style-type",1
"unsupported property list-style",1
"unsupported property line-clamp",1
"unsupported property letter-spacing",1
"unsupported property justify-self",1
"unsupported property justify-items",1
"unsupported property grid-template-rows",1
"unsupported property grid-template-columns",1
"unsupported property grid-row-gap",1 "unsupported property grid-row",1
"unsupported property grid-gap",1
"unsupported property grid-column-gap",1
"unsupported property grid-column",1
"unsupported property grid-auto-rows",1
"unsupported property grid-auto-flow",1
"unsupported property font-weigth",1
"unsupported property font-variant",1
"unsupported property font-feature-settings",1
"unsupported property font-display",1
"unsupported property clip-path",1
"unsupported property clear",1
"unsupported property border-collapse",1
"unsupported property background-origin",1
"unsupported property background-clip",1
"unsupported property background-blend-mode",1
"unsupported property background-attachment",1
"unsupported property appearance",1
"unrecoverable syntax error",1
"negative values of percentage are illegal",1 "invalid rule @charset",1
"invalid box shadow property.",1
```