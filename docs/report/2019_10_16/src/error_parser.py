

import re
import sys
import shlex
import pprint


def ParseReasons(file_name):
  pattern = re.compile('(?:\[[^\]]*\])(.*)')
  with open(file_name) as fd:
  	lines = fd.read().splitlines()

  reasons = {}

  for line in lines:
    if 'parser' in line:
      #print line
      #print shlex.split(line)
      m = pattern.match(line)
      if m:
        try:
          s = ' '.join(shlex.split(''.join(m.groups()))[2:])
          #print s
          if not s in reasons:
          	reasons[s] = 0;
          reasons[s] += 1
        except:
        	raise
  out = sorted(reasons.items(), key=lambda tup: (tup[1], tup[0]))
  out.reverse()
  return out


def Main(sys_args):
  out = ParseReasons(sys_args[0])
  for name, num in out:
    print('"%s"'%name+','+str(num))
  #pprint.pprint(ll)


if __name__ == '__main__':
	Main(sys.argv[1:])
	