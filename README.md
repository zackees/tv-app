![Minds.com](https://cdn-assets.minds.com/front/dist/en/assets/logos/logo.svg)

# TV App

## Brief

This contains an open source web-browser fork of chrome called cobalt. Cobalt is installed on many televisions. The ubuntu version will run like it does on a television and this is useful in a development cycle where the user wants to test how a web-app will work on a development machine. This repo contains a cobalt binary to run on ubuntu. We have provided two methods of running this, either directly with the binary or with a self contained virtual machine with cobalt running on it.

## Option 1: Cobalt binary for Testing on Ubuntu
 - If you have ubuntu then you can run the cobalt test app directly:
   - https://mega.nz/#!0I823IBb!WKjckEqudtCFssqJ0JlnTsZyfcIz3OnNfudCkkcQxxU
   - example cmd line for running minds.tv:
     - cd ~/cobalt/cobalt/src/out/linux-x64x11_devel
     - ./cobalt --url=https://minds.tv --allow_http --ignore_certificate_errors


## Option 2: Cobalt+VM for testing on any machine

This is an ubuntu vm instance that runs on Oracle's VitualBox. The advantage of this app is that it will run on any platform.
 - Get a decent laptop or desktop :D
 - Download Oracle's VirtualBox for your specific platform and install it.
 - Download the cobalt VM image (ova file) from the following address:
   - https://mega.nz/#!lINVkChC!-I2qNrnc3F6HNw5g289nBR8pQ7QEIyaksj33PLP4w98


#### Follow these instructions to get the Cobalt VM into your VirtualBox instance

 - At the Appliance Settings Step of the import, make sure you put a check in "Reinitialize the  MAC address of all network cards"
 - Start the imported virtual machine and let it boot.
 - Once the virtual machine is running, open the README.txt on theit's desktop for additional instructions on how to run the cobalt for
   - minds.tv
   - minds.com
   - subverse.net
 - Password for username "minds" is "minds".
 - /mnt/shared is the publically shared file within the VM. You'll have to configure the VirtualBox machine for a path for the folder share.


## Docs

 - Local
   - [Slide Deck Overview of how Minds can run a TV Channel](./docs/cobalt_minds_slide_deck.pdf)
   - [Compatibility Report - Oct 16th, 2019](./docs/report/2019_10_16/report.md)
   - [Expectations document](./docs/expectations.md)
 - External
   - [Build setup for cobalt](https://cobalt.foo/development/setup-linux.html)
   - [Cobalt Supportted HTML/JS/CSS featureset](https://cobalt.foo/development/reference/supported-features.html)

## Code Locations

 - The code for the frontend can be found here
   - https://gitlab.com/minds/front
 - Bundles and Sourcemaps are here
   - https://gitlab.com/minds/front/-/jobs/317085556/artifacts/browse/dist/en/
 - Cobalt code
   - https://cobalt.googlesource.com/cobalt


## Git Workflow

gitlab workflow:
* Go to the tv-app repo
  * https://gitlab.com/minds/tv-app
* Fork it
* On local drive, create a folder to hold local copy of the code
 * git init
 * git remote add origin https://gitlab.com/USER_NAME/tv-app
* git pull origin master
* git add .
* git commit -m "MESSAGE"
* git push origin master
* Then do a pull request